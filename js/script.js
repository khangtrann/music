(function ($) {
  "use strict";
  let currentMusicIndex = 0;
  const musicData = [
    {
      name: "Sài Gòn Đau Lòng Quá...「Lofi ver.」",
      slug: "sai_gon_dau_long_qua.mp3",
    },
    {
      name: "BINZ x ĐEN - CHO MÌNH EM",
      slug: "cho_minh_em.mp3",
    },
    {
      name: "13:00 am | NHA x Táo x Khói ~ Có Gió Và Mây, Blue Tequila,...",
      slug: "13_00_am_nha_x_tao_x_khoi.mp3",
    },
    {
      name: "Album PHẠM NGUYÊN NGỌC",
      slug: "album_pham_nguyen_ngoc_cam_on_ton_thuong.mp3",
    },
    {
      name: "Những giai điệu dang dở | NHA",
      slug: "nhung_giai_dieu_dang_do_nha.mp3",
    },
  ];

  window.audioControlService = (function AudioControlService() {
    const audioPlayer = $("#audio-player");
    const musicTitle = $("#music-title");

    const updatePlayer = () => {
      audioPlayer.attr("src", `./music/${musicData[currentMusicIndex].slug}`);
      musicTitle.text(musicData[currentMusicIndex].name);
    };

    const next = () => {
      if (currentMusicIndex < musicData.length - 1) {
        currentMusicIndex += 1;
      } else {
        currentMusicIndex = 0;
      }

      updatePlayer();
    };

    const previous = () => {
      if (currentMusicIndex > 0) {
        currentMusicIndex -= 1;
      } else {
        currentMusicIndex = musicData.length - 1;
      }

      updatePlayer();
    };

    return {
      next,
      previous,
    };
  })();

  // Windows load
  $(window).on("load", function () {
    // Site loader
    $(".loader-inner").fadeOut();
    $(".loader").delay(200).fadeOut("slow");

    // Audio play
    const audioPlayer = $("#audio-player");
    audioPlayer.prop("volume", 0.3);
    audioPlayer.on("ended", function () {
      audioControlService.next();
    });

    const musicTitle = $("#music-title");
    musicTitle.text(musicData[currentMusicIndex].name);

    // Handle for countdown
    const daysEl = document.getElementById("days");
    const hoursEl = document.getElementById("hours");
    const minutesEl = document.getElementById("minutes");
    const secondsEl = document.getElementById("seconds");

    const newYear = "1 Jan 2022";

    const countdown = () => {
      const newYearDate = new Date(newYear);

      const currentDate = new Date();
      if (currentDate.getTimezoneOffset() > 0) {
        currentDate.setTime(
          currentDate.getTime() + currentDate.getTimezoneOffset() * 60 * 1000
        );
      }

      const totalSeconds = (newYearDate - currentDate) / 1000;
      const days = Math.floor(totalSeconds / 3600 / 24);
      const hours = Math.floor(totalSeconds / 3600) % 24;
      const minutes = Math.floor(totalSeconds / 60) % 60;
      const seconds = Math.floor(totalSeconds % 60);

      daysEl.innerHTML = days;
      hoursEl.innerHTML = formatTime(hours);
      minutesEl.innerHTML = formatTime(minutes);
      secondsEl.innerHTML = formatTime(seconds);
    };

    const formatTime = (time) => (time < 10 ? `0${time}` : time);

    // initial call
    countdown();

    setInterval(countdown, 1000);
  });

  // Scroll to
  $("a.scroll").smoothScroll({
    speed: 800,
    offset: -60,
  });

  // Site navigation setup
  var header = $(".header"),
    pos = header.offset(),
    blockTop = $(".block-top");

  $(window).scroll(function () {
    if ($(this).scrollTop() > pos.top + 500 && header.hasClass("default")) {
      header.fadeOut("fast", function () {
        $(this).removeClass("default").addClass("switched-header").fadeIn(200);
        blockTop.addClass("active");
      });
    } else if (
      $(this).scrollTop() <= pos.top + 500 &&
      header.hasClass("switched-header")
    ) {
      header.fadeOut("fast", function () {
        $(this).removeClass("switched-header").addClass("default").fadeIn(100);
        blockTop.removeClass("active");
      });
    }
  });

  // Hero resize
  var mainHero = $(" .hero .main-slider .slides li");
  function mainHeroResize() {
    mainHero.css("height", $(window).height());
  }

  $(function () {
    mainHeroResize();
  }),
    $(window).resize(function () {
      mainHeroResize();
    });

  // Slider

  $(".main-slider").flexslider({
    animation: "fade",
    slideshow: true,
    directionNav: false,
    controlNav: true,
    pauseOnAction: false,
    animationSpeed: 1000,
  });

  $(".review-slider").flexslider({
    animation: "slide",
    slideshow: true,
    directionNav: true,
    controlNav: false,
    pauseOnAction: false,
    animationSpeed: 500,
  });

  $(".main-nav li a").on("click", function () {
    if ($(this).hasClass("mobile")) {
      nav.slideToggle();
      $(".toggle-mobile-but").toggleClass("active");
    }
  });

  // Append images as css background
  $(".background-img").each(function () {
    var path = $(this).children("img").attr("src");
    $(this)
      .css("background-image", 'url("' + path + '")')
      .css("background-position", "initial");
  });
})(jQuery);
